package day02;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

import com.db.DbConnection;




public class Demo06 {
    public static void main(String[] args) {
    	
    	// Select and display Employee details by ID
    	//get each employee data by empId
    	
        Connection connection = DbConnection.getConnection();
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter Employee ID to view details:");
        int empId = scanner.nextInt();
        System.out.println();

        String selectQuery = "SELECT * FROM employee WHERE empId = ?";

        try {
            preparedStatement = connection.prepareStatement(selectQuery);
            preparedStatement.setInt(1, empId);

            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                System.out.println("\nEmployee Details:");
                System.out.println("EmpId   : " + resultSet.getInt(1));
                System.out.println("EmpName : " + resultSet.getString(2));
                System.out.println("Salary  : " + resultSet.getDouble(3));
                System.out.println("Gender  : " + resultSet.getString(4));
                System.out.println("EmailId : " + resultSet.getString(5));
                System.out.println("Password: " + resultSet.getString(6));
            } else {
                System.out.println("No record found for EmployeeID: " + empId);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
