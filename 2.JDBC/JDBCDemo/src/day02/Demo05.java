package day02;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

import com.db.DbConnection;


public class Demo05 {
    public static void main(String[] args) {
    	
    	// delete an Employee Record
    	
        Connection connection = DbConnection.getConnection();
        PreparedStatement preparedStatement = null;

        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter EmployeeName to delete:");
        int empId = scanner.nextInt();
        System.out.println();

        String deleteQuery = "DELETE FROM employee WHERE empId=?";

        try {
            preparedStatement = connection.prepareStatement(deleteQuery);

            preparedStatement.setInt(1, empId);

            int result = preparedStatement.executeUpdate();

            if (result > 0) {
                System.out.println("Record Deleted...");
            } else {
                System.out.println("Record Deletion Failed!!!");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
