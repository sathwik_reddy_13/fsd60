package day02;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.db.DbConnection;

public class Demo07 {
	
	// fetch all the employees data
	
    public static void main(String[] args) {
        Connection connection = DbConnection.getConnection();
        Statement statement = null;
        ResultSet resultSet = null;

        try {
            statement = connection.createStatement();

            String fetchAllQuery = "SELECT * FROM employee";
            resultSet = statement.executeQuery(fetchAllQuery);

            while (resultSet.next()) {
                System.out.println("\nEmployee Details:");
                System.out.println("EmpId   : " + resultSet.getInt(1));
                System.out.println("EmpName : " + resultSet.getString(2));
                System.out.println("Salary  : " + resultSet.getDouble(3));
                System.out.println("Gender  : " + resultSet.getString(4));
                System.out.println("EmailId : " + resultSet.getString(5));
                System.out.println("Password: " + resultSet.getString(6));
                System.out.println("---------------------------");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
