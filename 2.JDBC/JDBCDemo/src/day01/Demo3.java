package day01;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import com.db.DbConnection;

public class Demo3 {

	//update employee name
	
	public static void main(String[] args) {
		Connection connection = DbConnection.getConnection();
		Statement statement = null;
		
		int empId = 107;
        String empName = "Pavan"; // Updated employee name
        double salary = 9898.98;
        String gender = "Male";
        String emailId = "utkarsh@gmail.com";
        String password = "123";

        String updateQuery = "update employee set empName='" + empName + "', salary=" + salary +
                ", gender='" + gender + "', emailId='" + emailId + "', password='" + password +
                "' where empId=" + empId;

        try {
            statement = connection.createStatement();
            int result = statement.executeUpdate(updateQuery);

            if (result > 0) {
                System.out.println(result + " Record(s) Updated...");
            } else {
                System.out.println("Record Update Failed...");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            if (connection != null) {
                statement.close();
                connection.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    

	}

}