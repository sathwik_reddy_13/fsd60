package day01;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

import com.db.DbConnection;

public class Demo07 {

    public static void main(String[] args) {
    	
    	// Fetch the employee record based on empId;
    	
        Connection connection = DbConnection.getConnection();

        try {
            fetchEmployeeRecord(connection);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private static void fetchEmployeeRecord(Connection connection) throws SQLException {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter Employee ID to fetch record: ");
        int empIdToFetch = scanner.nextInt();

        String selectQuery = "SELECT * FROM employee WHERE empId = ?";

        try (PreparedStatement statement = connection.prepareStatement(selectQuery)) {
            statement.setInt(1, empIdToFetch);

            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    int empId = resultSet.getInt("empId");
                    String empName = resultSet.getString("empName");
                    double salary = resultSet.getDouble("salary");
                    String gender = resultSet.getString("gender");
                    String emailId = resultSet.getString("emailId");
                    String password = resultSet.getString("password");

                    System.out.println("Employee Record:");
                    System.out.println("Employee ID: " + empId);
                    System.out.println("Name: " + empName);
                    System.out.println("Salary: " + salary);
                    System.out.println("Gender: " + gender);
                    System.out.println("Email ID: " + emailId);
                    //Its not recommended to print passwords in real applications
                } else {
                    System.out.println("Employee with ID " + empIdToFetch + " not found.");
                }
            }
        }
    }
}
