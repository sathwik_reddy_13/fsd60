package com.web;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dao.EmployeeDao;
import com.dto.Employee;

@WebServlet("/LoginServlet") 
public class LoginServlet extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String emailId = request.getParameter("emailId");
		String password = request.getParameter("password");
		
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		HttpSession session = request.getSession(true);
		session.setAttribute("emailId", emailId);

		out.print("<html>");
		out.print("<body >");
		if (emailId.equalsIgnoreCase("HR") && password.equals("HR")) {

			// Calling HRHomePage (Servlet)
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("HRHomePage.jsp");
			requestDispatcher.forward(request, response);

		}else if(emailId.equalsIgnoreCase("EMP") && password.equals("EMP")){
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("EmpHomePage.jsp");
			requestDispatcher.forward(request, response);
		}
		else{
			EmployeeDao employeeDao = new EmployeeDao();
			Employee employee = employeeDao.empLogin(emailId, password);

			if (employee != null) {
				
				//Storing Employee Object Under Session
				session.setAttribute("employee", employee);
							
				RequestDispatcher requestDispatcher = request.getRequestDispatcher("EmpHomePage.jsp");
				requestDispatcher.forward(request, response);

			} else {
				out.print("<h1 style='color:red'>Invalid Credentials</h1>");
				RequestDispatcher requestDispatcher = request.getRequestDispatcher("Login.html");
				requestDispatcher.include(request, response);
			}
		}
		out.print("</body>");
		out.print("</html>");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}

