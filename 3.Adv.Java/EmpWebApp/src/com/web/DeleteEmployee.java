package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.EmployeeDao;

/**
 * Servlet implementation class DeleteEmployee
 */
@WebServlet("/DeleteEmployee")
public class DeleteEmployee extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	response.setContentType("text/html");
    	PrintWriter out = response.getWriter();
    		
    	int empId = Integer.parseInt(request.getParameter("empId"));
    		
    	EmployeeDao employeeDao = new EmployeeDao();
    	int result = employeeDao.deleteEmployee(empId);
    		
    		
    	if (result > 0) {
    		request.getRequestDispatcher("GetAllEmployees").forward(request, response);
    	} else {
    		request.getRequestDispatcher("HRHomePage.jsp").include(request, response);
    		out.print("<br/>");
    		out.print("<h2 style='color:red;'>Unable to Delete the Employee Record!!!</h2>");
    	}
    		
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
