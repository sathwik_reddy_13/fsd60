package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/GreetUser")
public class GreetUser extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // Get the user's name from the request parameter
        String userName = request.getParameter("userName");

        // Generate the greeting message
        String greetingMessage = "Hello, " + userName + "!";

        // Set the content type of the response to HTML
        response.setContentType("text/html");

        // Get the PrintWriter object
        PrintWriter out = response.getWriter();

        // Write the HTML response with background color and text color styles
        out.print("<html><head><style>body {background-color: #cce5ff; color: #004085;}</style></head><body>");
        out.print("<h2 style='color: #155724;'>" + greetingMessage + "</h2>");
        out.print("</body></html>");
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        doGet(request, response);
    }
}
