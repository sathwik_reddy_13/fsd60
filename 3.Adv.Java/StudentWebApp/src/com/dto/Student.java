package com.dto;

public class Student {

 private int StudentId;
 private String StudentName;
 private String course;
 private String gender;
 private String MobileNumber;
 private String EmailId;
 private String password;
 
 
public Student() {
	super();
}

public Student(int studentId, String studentName, String course, String gender, String mobileNumber, String emailId,
		String password) {
	super();
	StudentId = studentId;
	StudentName = studentName;
	this.course = course;
	this.gender = gender;
	MobileNumber = mobileNumber;
	EmailId = emailId;
	this.password = password;
}


public int getStudentId() {
	return StudentId;
}


public void setStudentId(int studentId) {
	StudentId = studentId;
}


public String getStudentName() {
	return StudentName;
}


public void setStudentName(String studentName) {
	StudentName = studentName;
}


public String getCourse() {
	return course;
}


public void setCourse(String course) {
	this.course = course;
}


public String getGender() {
	return gender;
}


public void setGender(String gender) {
	this.gender = gender;
}


public String getMobileNumber() {
	return MobileNumber;
}


public void setMobileNumber(String mobileNumber) {
	MobileNumber = mobileNumber;
}


public String getEmailId() {
	return EmailId;
}


public void setEmailId(String emailId) {
	EmailId = emailId;
}


public String getPassword() {
	return password;
}


public void setPassword(String password) {
	this.password = password;
}





@Override
public String toString() {
	return "Student [StudentId=" + StudentId + ", StudentName=" + StudentName + ", course=" + course + ", gender="
			+ gender + ", MobileNumber=" + MobileNumber + ", EmailId=" + EmailId + ", password=" + password + "]";
}
 
 
}

