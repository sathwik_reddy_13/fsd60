package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.StudentDao;
import com.dto.Student;

@WebServlet("/GetStudentByName")
public class GetStudentByName extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        String studentName = request.getParameter("studentName");

        StudentDao studentdao = new StudentDao();
        Student student = studentdao.getStudentByName(studentName);

        out.print("<body bgcolor='lightyellow' text='green'>");

        RequestDispatcher requestDispatcher = request.getRequestDispatcher("StudentHomePage");
        requestDispatcher.include(request, response);

        if (student != null) {
            out.print("<table border='2' align='center'>");

            out.print("<tr>");
            out.print("<th> StudentId    </th>");
            out.print("<th> StudentName  </th>");
            out.print("<th> Course   </th>");
            out.print("<th> Gender   </th>");
            out.print("<th> MobileNumber   </th>");
            out.print("<th> Email-Id </th>");
            out.print("<th> password   </th>");
            out.print("</tr>");

            out.print("<tr>");
            out.print("<td>" + student.getStudentId() + "</td>");
            out.print("<td>" + student.getStudentName() + "</td>");
            out.print("<td>" + student.getCourse() + "</td>");
            out.print("<td>" + student.getGender() + "</td>");
            out.print("<td>" + student.getMobileNumber() + "</td>");
            out.print("<td>" + student.getEmailId() + "</td>");
            out.print("<td>" + student.getPassword() + "</td>");
            out.print("</tr>");

            out.print("</table>");
        } else {
            out.print("<h3 style='color:red;'>Unable to Fetch the Student Record</h3>");
        }

        out.print("</body>");

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        doGet(request, response);
    }

}

