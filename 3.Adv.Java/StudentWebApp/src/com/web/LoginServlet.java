package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.StudentDao;
import com.dto.Student;


@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
			
		String emailId = request.getParameter("emailId");
		String password = request.getParameter("password");
			
		out.print("<html>");
		out.print("<body bgcolor='lightyellow' text='blue'>");
		out.print("<center>");
			
		if (emailId.equalsIgnoreCase("mahender@gmail.com") && password.equals("1234")) {		
				
			//calling studenthome page 
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("StudentHomePage");
			requestDispatcher.forward(request, response);
				
		}else{
			StudentDao studentdao = new StudentDao();
			Student student = studentdao.studentlogin(emailId, password);
			if(student != null){
				RequestDispatcher requestDispatcher = request.getRequestDispatcher("StudentHomePage");
				requestDispatcher.forward(request, response);
			}
			else{
				out.print("<h1 style='color:red'>Invalid Credentials</h1>");
				
				//Calling Login.html 
				RequestDispatcher requestDispatcher = request.getRequestDispatcher("StudentLoginPage.html");
				requestDispatcher.include(request, response);
			}
		}
			
		out.print("<center></body></html>");
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
