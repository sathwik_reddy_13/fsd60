import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrl: './login.component.css'
})
export class LoginComponent {
  emailId: any;
  password: any;
  emp: any;

  constructor(private router: Router, private service: EmpService) {
  }

  async loginSubmit(loginForm: any) {

    localStorage.setItem('emailId', loginForm.emailId);
        
    if (loginForm.emailId == 'HR' && loginForm.password == 'HR') {      
      this.service.isUserLoggedIn();
      this.router.navigate(['showemps']);
    } else {      
      
      await this.service.employeeLogin(loginForm.emailId, loginForm.password).then((data: any) => {
        this.emp = data;
      });

      if (this.emp != null) {
        this.service.isUserLoggedIn();
        this.router.navigate(['products']);
      } else {
        alert('Invalid Credentials');
        this.router.navigate(['']);
      }
    }
  }
}
