import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

// Define an interface for cart items
interface CartItem {
  name: string;
  price: number;
  imgSrc: string;
  // Add any other properties as needed
}

@Injectable({
  providedIn: 'root'
})
export class CartService {
  private cartItems: CartItem[] = [];
  private cartSubject = new BehaviorSubject<CartItem[]>([]);

  constructor() {}

  getCartItems() {
    return this.cartSubject.asObservable();
  }

  addToCart(product: CartItem) {
    this.cartItems.push(product);
    this.cartSubject.next([...this.cartItems]);
  }

  removeItem(index: number) {
    this.cartItems.splice(index, 1);
    this.cartSubject.next([...this.cartItems]);
  }
  purchaseItem(index: number) {
    this.removeItem(index); // Remove the item from the cart
  }
}
