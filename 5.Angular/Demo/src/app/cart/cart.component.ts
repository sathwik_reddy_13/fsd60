import { Component } from '@angular/core';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrl: './cart.component.css'
})
export class CartComponent {

  cartProducts: any;
  data: any;
  emailId: any;
  total: any;

  constructor() {
    this.total = 0;
    this.emailId = localStorage.getItem('emailId');
    this.cartProducts = [];
    this.data = localStorage.getItem('cartProducts');
    this.cartProducts = JSON.parse(this.data);

    this.cartProducts.forEach((element: any) => {
      this.total = this.total + element.price;
    });

  }

}
