import { Component } from '@angular/core';

@Component({
  selector: 'app-test2',
  templateUrl: './test2.component.html',
  styleUrl: './test2.component.css'
})
export class Test2Component {

  person: any;

  constructor() {
    this.person = {
      id:101,
      name:'Harsha',
      age:22,
      address: {streetNo:101, city:'Hyd', state:'Telangana'},
      hobbies: ['Running', 'Eating', 'Music', 'Movies']
    }
  }

  submit() {
    alert('Button Clicked...');
    alert(this.person);
    alert("Id: " + this.person.id + "\nName:" + this.person.name);
    console.log(this.person);
  }

}
