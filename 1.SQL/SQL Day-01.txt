SQL Day-01
----------

SQL
---
1. DDL (Data Defination Language)     - Create, Alter, Drop
2. DML (Data Manipulate Language)     - Insert, Update, Delete
3. DQL (Data Query Language)          - Select
4. TCL (Transaction Control Language) - Commit, RollBack



Show All Databases
show databases;


Create MyOwn Database
create database fsd60;

Get into the fsd60 database
use fsd60;

show all tables from database
show tables;

clear the screen
system cls;


