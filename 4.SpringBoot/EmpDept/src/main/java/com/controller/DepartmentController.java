package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.DepartmentDao;
import com.model.Department;

@RestController
@CrossOrigin(origins="http://localhost:4200")
public class DepartmentController {

	@Autowired
	DepartmentDao departmentDao;
	
	@GetMapping("getAllDepartments")
	public List<Department> getAllDepartments() {
		return departmentDao.getAllDepartments();
	}
	
	@GetMapping("getDepartmentById/{deptId}")
	public Department getDepartmentById(@PathVariable("deptId") int deptId) {
		return departmentDao.getDepartmentById(deptId);
	}
		
	@GetMapping("getDepartmentByName/{deptName}")
	public Department getDepartmentByName(@PathVariable("deptName") String deptName) {
		return departmentDao.getDepartmentByName(deptName);
	}
	
	@PostMapping("addDepartment")
	public Department addDepartment(@RequestBody Department department) {
		return departmentDao.addDepartment(department);
	}
	
	@PutMapping("updateDepartment")
	public Department updateDepartment(@RequestBody Department department) {
		return departmentDao.updateDepartment(department);
	}
	
	@DeleteMapping("deleteDepartmentById/{deptId}")
	public String deleteDepartmentById(@PathVariable("deptId") int deptId) {
		departmentDao.deleteDepartmentById(deptId);
		return "Department with deptId: " + deptId + ", Deleted Successfully!!!";
	}
}
