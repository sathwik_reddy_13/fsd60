package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.ProductDao;
import com.model.Product;

@RestController
public class ProductController {
	
	@Autowired
	ProductDao productDao;

	@GetMapping("getAllProducts")
	public List<Product> getAllProducts() {		
		List<Product> productList = productDao.getAllProducts();		
		return productList;
	}

	@GetMapping("getProductById/{id}")
	public Product getProductById(@PathVariable("id") int prodId) {
		Product product = productDao.getProductById(prodId);
		return product;
	}
	
	@GetMapping("getProductByName/{pName}")
	public List<Product> getProductByName(@PathVariable("pName") String prodName) {	
		List<Product> productList = productDao.getProductByName(prodName);		
		return productList;
	}
	
	@PostMapping("addProduct")
	public Product addProduct(@RequestBody Product product) {
		Product prod = productDao.addProduct(product);
		return prod;
	}
	
	@PutMapping("updateProduct")
	public Product updateProduct(@RequestBody Product product) {
		Product prod = productDao.updateProduct(product);
		return prod;
	}
	
	@DeleteMapping("deleteProductById/{id}")
	public String deleteProductById(@PathVariable("id") int prodId) {
		productDao.deleteProductById(prodId);
		return "Product with ProductId: " + prodId +", Deleted Successfully";
	}

}


